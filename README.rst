==========
simplehelp
==========

SimpleHelp Formula creates and configures a SimpleHelp server.

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.


Available states
================

.. contents::
    :local:

``simplehelp``
--------------

TODO - add description of this state

``simplehelp.install``
-------------------

Handles installation of simplehelp

``simplehelp.config``
-------------------

Handles configuration of simplehelp


``simplehelp.client``
---------------------

Installs simplehelp remote access

``simplehelp.technician``
-------------------------

Installs the simplehelp technician console

Template
========

This formula was created from a cookiecutter template.

See https://github.com/mitodl/saltstack-formula-cookiecutter.
