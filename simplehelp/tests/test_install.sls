{% from "simplehelp/map.jinja" import simplehelp with context %}

{% for pkg in simplehelp.pkgs %}
test_{{pkg}}_is_installed:
  testinfra.package:
    - name: {{ pkg }}
    - is_installed: True
{% endfor %}
