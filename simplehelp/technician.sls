{% from "simplehelp/map.jinja" import simplehelp with context %}

{% if grains.cpuarch in ['x86_64'] %}

simplehelp_opt_dir_for_technician_exists:
  file.directory:
    - name: /opt/simplehelp-technician
    - user: root
    - group: root
    - makedirs: True

simplehelp_technician_download_installer:
  file.managed:
    - name: /opt/simplehelp-technician/simplehelp-technician.tar
    - source: http://{{ simplehelp.server.url }}/technician/SimpleHelp%20Technician-linux64-online.tar?language=gb&hostname=http%3A%2F%2F{{ simplehelp.server.url }}&ie=ie.exe
    - source_hash: {{ simplehelp.technician.technician_app_hash }}
    - user: root
    - group: root
    - mode: 0664

simplehelp_technician_extract_installer:
  archive.extracted:
    - name: /opt/simplehelp-technician/installer-technician
    - source: /opt/simplehelp-technician/simplehelp-technician.tar
    - enforce_toplevel: False
    - user: root
    - group: root
    - require:
      - simplehelp_technician_download_installer

{% for user in simplehelp.technician.install_for %}
{% if user in salt['user.list_users']() %}
simplehelp_technician_install_installer_for_{{ user }}:
  cmd.run:
    - name: /opt/simplehelp-technician/installer/SimpleHelp\ Technician-linux64-online
    - runas: {{ user }}
    - bg: True
    - require:
      - simplehelp_technician_download_installer
      - simplehelp_technician_extract_installer
    - unless:
      - 'ls /home/{{ user }}/.JWrapper/JWrapper-SimpleHelp\ Technician'
{% endif %}
{% endfor %}
{% endif %}
