{% from "simplehelp/map.jinja" import simplehelp with context %}

include:
  - .service

/opt:
  file.directory:
    - name: /opt
    - user: root
    - group: root

{% if grains.cpuarch in ['x86_64'] %}
download-simplehelp-installer:
  file.managed:
    - name: '/tmp/simplehelp.tar.gz'
    {% if grains.cpuarch == 'x86_64' %}
    - source: {{ simplehelp.simplehelp_amd64_url }}
    - source_hash: {{ simplehelp.simplehelp_amd64_hash }}
    {% endif %}
    - user: root
    - group: root
    - mode: 0664
    - unless:
      - 'curl http://localhost/version | grep {{ simplehelp.simplehelp_version }}'
{% endif %}

{#
extract-simplehelp-to-opt:
  archive.extracted:
    - name: /opt
    - source: /tmp/simplehelp.tar.gz
    - user: root
    - group: root
    - require:
      - file: download-simplehelp-installer
#}
