Templates
=========

Put your templates here.

They should be relevant to simplehelp-formula and should be used
to generate files using a template engine (e.g. Jinja).
