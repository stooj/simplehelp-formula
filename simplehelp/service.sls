{% from "simplehelp/map.jinja" import simplehelp with context %}

simplehelp-systemd-script:
  file.managed:
    - name: /etc/systemd/system/simplehelp.service
    - source: salt://simplehelp/files/etc/systemd/system/simplehelp.service
    - user: root
    - group: root
    - mode: 644

simplehelp_service_running:
  service.running:
    - name: simplehelp
    - enable: True
    - require:
      - file: simplehelp-systemd-script
