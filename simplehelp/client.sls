{% from "simplehelp/map.jinja" import simplehelp with context %}

simplehelp_opt_dir_for_client_exists:
  file.directory:
    - name: /opt
    - user: root
    - group: root

{% if grains.cpuarch in ['x86_64'] %}
simplehelp_client_download_installer:
  file.managed:
    - name: /tmp/simplehelp-remote-access.tar
    - source: http://{{ simplehelp.server.url }}/access/Remote%20Access-linux64-online.tar?language=gb&app={{ simplehelp.client.remote_access_app_id }}&hostname=http%3A%2F%2F{{ simplehelp.server.url }}&ie=ie.exe
    - source_hash: {{ simplehelp.client.remote_access_app_hash }}
    - user: root
    - group: root
    - mode: 0664
    - unless:
      - 'ls /opt/JWrapper-Remote\ Access/Remote\ AccessLinLauncher64'

simplehelp_client_extract_installer:
  archive.extracted:
    - name: /tmp/simplehelp
    - source: /tmp/simplehelp-remote-access.tar
    - enforce_toplevel: False
    - user: root
    - group: root
    - require:
      - simplehelp_client_download_installer
    - unless:
      - 'ls /opt/JWrapper-Remote\ Access/Remote\ AccessLinLauncher64'

simplehelp_client_run_installer:
  cmd.run:
    - name: /tmp/simplehelp/Remote\ Access-linux64-online
    - require:
      - simplehelp_client_download_installer
      - simplehelp_client_extract_installer
    - unless:
      - 'ls /opt/JWrapper-Remote\ Access/Remote\ AccessLinLauncher64'
{% endif %}
